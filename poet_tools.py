"""
Contains several functions for estimating the 'poetic quelity' of a text.

should be initialised by calling 'prondict_to_redis(cmudict.dict(),r_server,key_format)
"""

from nltk.corpus import cmudict
from nltk.tokenize import word_tokenize as tokenize
import re



def prondict_to_redis(prondict,r,key_format):
	"""Saves prondict to redis db"""
	for k,v in prondict.items():
		if not r.exists(key_format%k):
			#TODO currently assumes first
			#listed pronunciation to be the only one
			for i in v[0]:
				r.rpush(key_format%k,i)

def get_pron(word,r,key_format):
	"""gets word pronunciation"""
	return r.lrange(key_format%word,0,-1) or ['?%s%s' % (word,n) for n in estimate_stress(word)]

def get_sent_pron(sent,r,key_format):
	"""Concatonates pron for each word in sentance"""
	stress = []
	for word in [ w.lower() for w in tokenize(sent) if w.isalpha()]:
		stress += get_pron(word,r,key_format)
	return stress

#extracts stress information by discarding anything other than a digit
get_stress = lambda pron: "".join(i for i in "".join(pron) if i.isdigit())
			
def get_rhyme(pron,syls):
	""" takes a pron list and a number of syllables.
		returns the last sylables from the pron list"""
	end_syls = []
	n = syls
	for i in reversed(pron):
		if re.search(r'\d',i):
			n -= 1
		end_syls.append(i)
		if n < 1:
			end_syls.reverse()
			return end_syls
	end_syls.reverse()
	end_syls = ['']*n + end_syls
	return end_syls
		

def estimate_stress(word):
	""" Estimates (crudely) the stress of unknown words.
	"""
	if word.endswith('e'):
		word = word[:-1]
	if word[0] in 'aeiouy':
		syls = 1
		vowel = True
	else:
		syls = 0
		vowel = False
	for l in word:
		if not vowel and l in 'aeiouy':
			vowel = True
			syls += 1
		elif vowel:
			vowel = False
	return '1010101010101010101010101010101'[:syls or 1]



def compare(line1,line2,r,key_format):
	"""takes two lines as arguments and compares them according to
	the similarity of their stress patterns and rhyming of final
	phonetic elements.
	
	As it stands, all tinkering of algorithms should take place in this function.
	"""
	pron1 = get_sent_pron(line1,r,key_format)
	pron2 = get_sent_pron(line2,r,key_format)
	rhyme_score = compare_rhyme(pron1,pron2,1)*0.5
	rhyme_score += compare_rhyme(pron1,pron2,2)*0.25
	rhyme_score += compare_rhyme(pron1,pron2,3)*0.25
	stress_score = compare_stress(pron1,pron2)
	return rhyme_score * stress_score

def compare_rhyme(pron1,pron2, syls):
	""" returns a float from 0-1 representing the similarity
	of the lasy int(syls) syllables of the lines """
	rhyme1 = get_rhyme(pron1, syls)
	rhyme2 = get_rhyme(pron2, syls)
	rhyme1.reverse()
	rhyme2.reverse()
	length = min(len(rhyme1),len(rhyme2))
	score = 0.0
	for i in range(length):
		if rhyme1[i] in  rhyme2[max(i-1,0):min(i+2,length)]:
			score += 1
	score /= length
	return score
		
def compare_stress(pron1,pron2, offset=1):
	""" A wrapper function which compares two stress strings 
	( or in theory any strings) with various offsets and returns a score
	(a float from 0 - 1)for the closest match"""
	stress1 = get_stress(pron1)
	stress2 = get_stress(pron2)
	return max([
			compare_string(stress1,'?'*o+stress2) 
			for o in range(offset+1)
			])

def compare_string(string1,string2):
	""" Compares two strings and returns a similarity score between 0 and 1
	"""
	length = min(len(string1),len(string2))
	return sum([1.0 for i in range(length) if string1[i] == string2[i]])/length
