"""
This is a simple example of WebSocket + Tornado + Redis Pub/Sub usage.
Do not forget to replace YOURSERVER by the correct value.
Keep in mind that you need the *very latest* version of your web browser.
You also need to add Jacob Kristhammar's websocket implementation to Tornado:
Grab it here:
    http://gist.github.com/526746
Or clone my fork of Tornado with websocket included:
    http://github.com/pelletier/tornado
Oh and the Pub/Sub protocol is only available in Redis 2.0.0:
    http://code.google.com/p/redis/downloads/detail?name=redis-2.0.0-rc4.tar.gz

Tested with Chrome 6.0.490.1 dev under OS X.

For questions / feedback / coffee -> @kizlum or thomas@pelletier.im.
Have fun.

Modified by Lorenzo Bolla on 11 Feb 2013:
    Threading done right: don't call Tornado's handlers functions in threads, but use add_callback
    Updated to redis 2.6.9
    Tested with Py3k

"""
from functools import partial
import threading
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import tornado.template as template
import tornado.options
import redis
import simplejson
from poet_tools import compare

from tornado.options import define, options

define("port",default=8000,help="run on the given port", type=int)

loader = template.Loader('templates')
LISTENERS = []


def redis_listener():
    """subscribe to channel 'test_realtime', listen for messages, and
    broadcast to all LISTENERS"""
    r = redis.Redis()
    ps = r.pubsub()
    ps.subscribe('test_realtime')
    io_loop = tornado.ioloop.IOLoop.instance()
    for message in ps.listen():
        for element in LISTENERS:
            io_loop.add_callback(partial(element.on_message, message))


class NewMsgHandler(tornado.web.RequestHandler):
    """handles requests from url r'/'"""
    def get(self):
		#sends html
        self.write(loader.load('index.html').generate(port=options.port))

    def post(self):
		#receives data, tests wether it rhymes, and
		# if so publishes to 'test_realtime' channel
        data = self.get_argument('data')
        r = redis.Redis()
        message = simplejson.loads(data)['text']
        previous_message = r.get('line:%s' % (r.get('LastLineIndex')))
		#if line is second line of a couplet
        if int(r.get('LastLineIndex')) % 2 == 1:
			#compare it to the previous line. If its score is high enough
			#save and publish
            if compare(message,previous_message,r,'prondict:%s') > 0.5:
                r.incr('LastLineIndex')
    	        r.set('line:%s'%r.get('LastLineIndex'),message)
                r.publish('test_realtime', data)
            else:
				#if comparison fails, send warning from Server
				# to user who sent message
				id = simplejson.loads(data)['id']
				l = [l for l in LISTENERS if l.id == id ][0]
				text = '<p class="server_message">You need to match rhyme and metre with <b><em>%s</e></b></p>'%(previous_message)
				message = simplejson.dumps({'type': 'console','text': text})
				l.write_message(message)
        else:
			#if first like of a couplet accept, save, and publish
            r.incr('LastLineIndex')
            r.set('line:%s'% r.get('LastLineIndex'),message)
            r.publish('test_realtime',data)


class RealtimeHandler(tornado.websocket.WebSocketHandler):
    """handles requests to r'/realtime/'
    (exclusively via client websocket)"""
	
	#Adds self to to listeners
    def open(self):
        r = redis.Redis()
        r.incr('Listeners')
        self.id = r.get('Listeners')
        LISTENERS.append(self)
        message = simplejson.dumps({ 'type' : 'id', 'id': self.id});
        self.write_message(message)

    def on_message(self, message):
		#called by redis_listener.
		#parses JSON and writes html string 
        data = simplejson.loads(message['data'])
        text = "<b>%s</b>: %s"%(data['name'], data['text'])
        message = simplejson.dumps({ 'type' : 'message' , 'text' : text })
        self.write_message(message)

    def on_close(self):
		#removes self from listeners
        LISTENERS.remove(self)


settings = {
    'auto_reload': True,
}

application = tornado.web.Application([
    #handles requests
    (r'/', NewMsgHandler),
    (r'/realtime/', RealtimeHandler),
], **settings)


if __name__ == "__main__":
    tornado.options.parse_command_line()
    r = redis.Redis()
    if not r.exists('LastLineIndex'):
        r.set('LastLineIndex', 0)
        r.set('LastVerseIndex', 0)
        r.set('Listeners',0)
    threading.Thread(target=redis_listener).start()
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
